﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyReactiveFormAPI.Entities
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>().HasData(new Author
            {

                AuthorId = Guid.NewGuid(),
                FirstName = "nik",
                LastName = "rathod",
                Genre = "Drama"

            }, new Author
            {
                AuthorId = Guid.NewGuid(),
                FirstName = "vivek",
                LastName = "rathod",
                Genre = "Fantasy"
            });
        }
    }
}
