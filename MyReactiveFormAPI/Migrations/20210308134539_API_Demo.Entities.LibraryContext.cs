﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyReactiveFormAPI.Migrations
{
    public partial class API_DemoEntitiesLibraryContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Author",
                columns: new[] { "AuthorId", "FirstName", "Genre", "LastName" },
                values: new object[] { new Guid("1495055a-7418-46b2-b995-8d543b97c90e"), "nik", "Drama", "rathod" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Author",
                columns: new[] { "AuthorId", "FirstName", "Genre", "LastName" },
                values: new object[] { new Guid("62d7ee38-1093-44c4-9d15-72422c287350"), "vivek", "Fantasy", "rathod" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "dbo",
                table: "Author",
                keyColumn: "AuthorId",
                keyValue: new Guid("1495055a-7418-46b2-b995-8d543b97c90e"));

            migrationBuilder.DeleteData(
                schema: "dbo",
                table: "Author",
                keyColumn: "AuthorId",
                keyValue: new Guid("62d7ee38-1093-44c4-9d15-72422c287350"));
        }
    }
}
